## Introductie

Met dit dashboard kunt u gemeentelijke vacaturesites doorzoeken. Daarvoor wordt
een handmatige lijst met vacaturesites gebruikt. Het dashboard probeert 
vervolgens zo zorgvuldig mogelijk de vacatures uit deze sites te destilleren. 
In sommige gevallen laat de vacaturesite maar een gedeelte van alle vacatures
zien, of is het lastig de vacatures automatisch te herkennen. Deze beperkingen
dienen in ogenschouw te worden genomen bij het gebruik van dit dashboard.
Het dashboard gebruikt de gemeente indeling uit 2022.

In het dashboard kunt u een provincie selecteren en een zoekterm invullen.
Wanneer u op de 'zoek' knop drukt, begint het systeem de vacaturesites te 
doorzoeken. Omdat het doorzoeken enige tijd in beslag neemt, kunt u maar een
provincie tegelijk selecteren. 

De kaart op het dashboard laat het aantal gevonden vacatures per gemeente zien.
Sommige gemeenten hebben geen aparte vacaturesite. Andere gemeenten hebben geen
site die automatisch te doorzoeken is. Deze gemeenten blijven grijs.

In de tabel is meer detail te zien over de gevonden vacatures. Per gemeente 
staat hier de url van de vacaturesite, de zoekterm en de gevonden vacature.

*Dit is werk in ontwikkeling!*

**Vragen of feedback?** Mail naar <gripopinformatie@vng.nl>.
