[![Creative Commons-Licentie](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)][cc_licentie]

Dit werk valt onder een [Creative Commons Naamsvermelding-NietCommercieel-GelijkDelen 4.0 Internationaal-licentie][cc_licentie]

[cc_licentie]: https://creativecommons.org/licenses/by-nc-sa/4.0/deed.nl
