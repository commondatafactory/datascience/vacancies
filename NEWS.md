# vacancies 0.1.0

* Having job-categories
* Script to check existence of url's
* Change table-text language to Dutch
* Filter table on municipalities with vacancies

# vacancies 0.0.1

* Starting dashboard at 'Flevoland'.

# vacancies 0.0.0.9000

* Added a `NEWS.md` file to track changes to the package.
