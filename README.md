
<!-- README.md is generated from README.Rmd. Please edit that file -->

# vacancies

<!-- badges: start -->
<!-- badges: end -->

Het doel van dit pakket is om gemeentelijke vacatures in kaart te
brengen. Daartoe haalt dit pakket alle gemeentelijke vacatures van
internet en maakt deze inzichtelijk op een landkaart en in een tabel via
een shiny-app.

## Installation

Het pakket van ‘vacancies’ kan geïnstalleerd worden vanuit GitLab als:

``` r
install.packages("devtools")

# Install the development version from GitLab
devtools::install_gitlab("https://gitlab.com/commondatafactory/datascience/vacancies") 
```

## Running the code

Na installatie van het pakket kan het dashboard worden gedraaid door
`run_app()` uit te voeren, zie voorbeelden hieronder.

## Example

Hieronder staat hoe de diverse dashboards kunnen worden opgestart.

``` r
library(vacancies)

# launch the dashboard with the vacancies overview
run_app()
```

# Doorontwikkelen

De code kan uiteraard ook van Gitlab gecloned worden, bijvoorbeeld als
`git clone https://gitlab.com/commondatafactory/datascience/vacancies.git`.
De brondata is opgenomen in de folder `inst/extdata`. Dit bestand bevat
de url’s naar de gemeentelijke websites voor de vacatures. Deze data kan
in de juiste formaat worden omgezet door:

- Het uitvoeren van het script `raw-data/create_dashboard_data.R`. Deze
  code neemt de bovengenoemde dataset en zet deze in het juiste formaat
  in de folder `inst/dashboard/data`. Van hieruit laadt de shinyapp de
  data en gebruikt deze.
- Het uitvoeren van het script `raw-data/check_urls.R`. In de loop van
  de tijd kunnen de opgeslagen url’s niet meer werkzaam zijn. Dit script
  checkt of de url’s nog altijd toegangelijk zijn en geeft een kort
  resultaat terug. Aan de hand van dit resultaat kan het bronbestand
  worden bijgewerkt.

## Licentie

<a rel="license" href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.nl"><img alt="Creative Commons-Licentie" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />Dit
werk valt onder een
<a rel="license" href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.nl">Creative
Commons Naamsvermelding-NietCommercieel-GelijkDelen 4.0
Internationaal-licentie</a>.
