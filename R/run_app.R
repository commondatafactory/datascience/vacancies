#' Run the vacancies dashboard
#'
#' @param ... other parameters
#'
#' @return nothing
#'
#' @import shiny
#' @importFrom magrittr %>%
#' @importFrom rlang .data
#' @importFrom stats setNames
#' @importFrom utils read.csv2
#'
#' @export
#'
#' @examples
#' \dontrun{
#' run_app()
#' }
#'

run_app <- function(...) {
  # This is the vacancies dashboard

  ##################################### Data preparatie #############################################

  # load the url list
  df.url <- read.csv2(
    system.file(
      file.path("dashboard", "data", "urls.csv"),
      package="vacancies"
    ),
    strip.white = TRUE,
    encoding = "UTF-8"
  )


  # load the synoniemen
  df.synoniem <- read.csv2(
    system.file(
      file.path("dashboard", "data", "synoniemen.csv"),
      package="vacancies"
    ),
    strip.white = TRUE,
    encoding = "UTF-8"
  )


  # load Nederlandse gemeenten en provincies
  gebieden <- get_Nederlandse_gebieden()

  # repair municipality names
  df.url[df.url == "s-Gravenhage"] = "'s-Gravenhage"
  df.url[df.url == "s-Hertogenbosch"] = "'s-Hertogenbosch"

  # keep only entries with a valid dutch municipality name
  df.url <- df.url %>%
    dplyr::filter(.data$gemeente %in% gebieden$Gemeentenaam)

  # assert that there are 345 municipalities
  if (nrow(df.url) != 345) {
    stop("Wrong number of municipalities, check the urls.csv file.")
  }

  # set the parameters
  parameters <- list(
    provincies = gebieden %>%
      dplyr::select(.data$Provincienaam, .data$Provinciecode) %>%
      unique() %>%
      dplyr::arrange(.data$Provincienaam),
    initial_provincie = "PV24",
    functiegroepen = df.synoniem %>%
      dplyr::arrange(.data$functiegroep) %>%
      dplyr::pull(.data$functiegroep) %>%
      unique()
  )

  # gemeentelijst
  initial_gemeenten <- as.vector(
    gebieden %>%
      dplyr::filter(.data$Provinciecode == parameters[["initial_provincie"]]) %>%
      dplyr::pull(.data$Gemeentenaam)
  )

  # load the municipality boundaries
  grensindeling <- get_grensindeling(peiljaar=peiljaar)

  # determine the position of 'www' directory
  www <- system.file(file.path("dashboard", "www"), package="vacancies")

  addResourcePath("www", system.file(file.path("dashboard", "www"), package="vacancies"))


  ##################################### JS gedeelte #################################################

  # script voor lege regel in index tabel
  # let op, deze functie wordt blijkbaar op alle tabellen toegepast.
  jsc <- DT::JS(
    "function(settings, json) {",
    "$('#DataTables_Table_0_wrapper > .dt-buttons').appendTo('#download_button');",
    "}"
  )


  ##################################### UI gedeelte #################################################

  # Peter's design
  ui <- fillPage(
    tags$head(tags$title("Gemeentelijke vacatures")),
    tags$head(tags$link(rel="shortcut icon", href="www/favicon.ico")),
    # Include tracking code ----
    tags$head(includeHTML(file.path(www, "geitjes-analytics.html"))),
    shinyjs::useShinyjs(),
    includeCSS(file.path(www, "vng_gmsd_flex.css")),
    div(
      id="visualisatie vacancies",
      class="flex container",
      leftPanel(parameters),
      rightPanel()
    )
  )

  ######################################## server gedeelte ##########################################

  # Define server logic
  server <- function(input, output, session) {

    # Init loaders
    loader_replace <- spsComps::addLoader$new(
      "go_search",
      type = "facebook",
      id = "waiting_animation"
    )


    # observe events
    observeEvent(input$terug_van_toelichting, {
      shinyjs::click("colofon_link")
    })


    observeEvent(input$logo, {
      shinyjs::click("colofon_link")
    })


    # refresh if clicked
    observeEvent(input$refresh, {
      shinyjs::refresh()
    })


    # action on search term
    observeEvent(input$go_search, {
      search_term(input$search_query)
    })


    # action if provincie is selected
    observeEvent(
      input$provinciecode,
      {
        gemeenten(
          as.vector(
            gebieden %>%
              dplyr::filter(.data$Provinciecode == input$provinciecode) %>%
              dplyr::pull(.data$Gemeentenaam)
          )
        )
      },
      ignoreInit = TRUE
    )


    # vervang de data in de tabel
    observe({

      DT::replaceData(
        proxy = proxy_lijst_resultaten,
        data = df.results() %>%
          dplyr::filter(.data$functie != ""),
        rownames = FALSE,
        resetPaging = FALSE
      )

      # stop with loading animation
      loader_replace$hide()
    })



    # reactives

    # reactive value with the search term
    search_term <- reactiveVal("")

    # lijst of gemeenten
    gemeenten <- reactiveVal(initial_gemeenten)


    # data.frame with raw results
    df.results <- reactive({

      # show loader icon
      loader_replace$show()

      # set variables
      out <- data.frame()

      # compose the search term
      search_string <- paste(
        df.synoniem %>%
          dplyr::filter(.data$functiegroep == search_term()) %>%
          dplyr::pull(.data$functie),
        collapse="|"
      )


      # loop over gemeenten
      for (gemeente in gemeenten()) {
        if (gemeente %in% df.url$gemeente) {
          # er is een handmatige url beschikbaar
          if (df.url[df.url$gemeente == gemeente, "automatisch"] == "ja") {
            # de url kan automatisch doorzocht worden
            url <- data.frame(
              gemeente = gemeente,
              url = sub(
                "zoektermhierinvullen",
                search_string,
                df.url[df.url$gemeente == gemeente, "url"],
              ),
              hit_order = 0
            )
          } else {
            # automatische doorzoeking van url is niet mogelijk
            next
          }
        } else {
          # geen handmatige url, zoek de url dmv startpage
          url <- find_vacancy_url(gemeente)
        }
        out <- rbind(out, url)
      }

      # Create a Progress object
      progress <- shiny::Progress$new(min=0, max=nrow(out))
      progress$set(message = "Vacatures zoeken...", value = 0)
      # Close the progress when this reactive exits (even if there's an error)
      on.exit(progress$close())

      # Create a closure to update progress.
      # Each time this is called:
      # - If `value` is NULL, it will move the progress bar 1/5 of the remaining
      #   distance. If non-NULL, it will set the progress to that value.
      # - It also accepts optional detail text.
      updateProgress <- function(value = NULL, detail = NULL) {
        if (is.null(value)) {
          value <- progress$getValue()
          value <- value + 1
        }
        progress$set(value = value, detail = detail)
      }

      # build data.frame
      out <- out %>%
        dplyr::rowwise() %>%
        dplyr::mutate(
          zoekterm = {{search_string}},
          functiegroep = isolate(search_term()),
          resultaat = search_vacancy(url, search_string, updateProgress)
        )

      # turn url into hyperlink
      out$url <- paste0(
        "<a href='", out$url, "' target='_blank' rel='noopener noreferrer'>",
        out$url,
        "</a"
      )


      # every result in a separate row
      out <- out %>%
        tidyr::separate_rows(.data$resultaat, sep=";")



      # add function to data-frame
      out <- merge(out, df.synoniem, all.x=TRUE) %>%
        dplyr::rowwise() %>%
        dplyr::filter(
          .data$resultaat=="" | grepl(.data$functie, .data$resultaat, ignore.case = TRUE)
        ) %>%
        dplyr::mutate(
          functie = dplyr::if_else(.data$resultaat=="", "", .data$functie)
        ) %>%
        unique() %>%
        dplyr::select(
          .data$gemeente,
          .data$url,
          .data$hit_order,
          .data$zoekterm,
          .data$functiegroep,
          .data$functie,
          .data$resultaat
        )

      return(out)
    })


    # data.frame with aggregated results
    df.aggregated <- reactive({
      if (input$go_search == 0) {
        out <- data.frame(gemeente=character(), aantal=integer())
      } else {
        out <-df.results() %>%
          dplyr::distinct(.data$gemeente, .data$resultaat) %>%
          dplyr::group_by(gemeente) %>%
          dplyr::summarise(aantal = sum(.data$resultaat != ""))
      }
      return(out)
    })


    # geographic map
    koppel_geo_info_NL <- reactive({
      "
      Voorbereiding van data voor de kaartweergave en koppeling met geo-data.
      Selecteer alleen relevante kolommen en relevante rijen (afhankelijk wat op dashboard is gedaan)
      "

      grensindeling_result <- grensindeling %>%
        dplyr::left_join(df.aggregated(), by = c('statnaam' = 'gemeente')) %>%
        dplyr::mutate(
          weight = dplyr::if_else(.data$aantal>0, 1, 0.5, missing=0.5),
          fillColour = dplyr::if_else(.data$aantal>0, vng_marineblauw, "white", missing="lightgrey"),
          fillOpacity = 1
        )

      return(grensindeling_result)
    })


    # text-output

    # print het aantal vacatures en gemeenten
    output$hoofdPanel_text <- renderText(
      if (nrow(df.aggregated()) > 0) {
        sprintf(
          "%d vacatures in %d gemeenten",
          sum(df.aggregated()$aantal),
          sum(df.aggregated()$aantal > 0)
        )
      } else {
        "Maak uw keuze en druk op de 'zoek' knop"
      }
    )



    # kaarten


    output$kaart_NL <- leaflet::renderLeaflet({
      "
      Hier wordt de kaart van Nederland opgezet.
      Alle gemeenten in NL worden getoond.
      "

      # Maak de kaart met bijbehorende controls, zoals legenda, huidig geselecteerd jaar, hovers en highlights.
      grensindeling_selection <- koppel_geo_info_NL()
      label <- with(grensindeling_selection, paste0(
        "<p><h5>", statnaam,"</h5></br>
        <b>Aantal vacatures: </b>", round(aantal, 0),"</br>","</p>")
      )

      # Maak de kaart met bijbehorende controls, zoals legenda, huidig geselecteerd jaar, hovers en highlights.

      map1 <- leaflet::leaflet(
        data = grensindeling_selection,
        options = leaflet::leafletOptions(
          attributionControl = FALSE,
          zoomControl = TRUE,
          zoomSnap = 0.25,
          zoomDelta = 0.25,
          doubleClickZoom = FALSE,
          scrollWheelZoom = TRUE,
          dragging = TRUE
        ),
        height = "100vh"
      ) %>%
        leaflet::addPolygons(
          layerId=~statcode,
          color="black",
          weight=~weight,
          opacity=1,
          fillColor = ~fillColour,
          fillOpacity = ~fillOpacity,
          smoothFactor = 0.5,
          label=lapply(label, HTML),
          labelOptions = leaflet::labelOptions(textsize='13px'),
          highlightOptions = leaflet::highlightOptions(
            fillColor = vng_donkerblauw,
            bringToFront = TRUE
          )
        )

      # stop with loading animation
      loader_replace$hide()

      # return the map
      map1
    })



    # tables
    output$lijst_resultaten <- DT::renderDT({

      DT::datatable(
        isolate(
          df.results() %>%
            dplyr::filter(.data$functie != "")
        ),
        class = "hover",
        rownames = FALSE,
        escape = 1,
        selection = 'none',
        extensions = c("Buttons", "FixedColumns"),
        options = list(
          dom = 'Brtip',
          buttons = list(
            list(
              extend = 'collection',
              buttons = c('csv', 'excel', 'pdf'),
              text = 'Download deze tabel als:'
            )
          ),
          language = list(url = '//cdn.datatables.net/plug-ins/1.10.11/i18n/Dutch.json'),
          initComplete = jsc,
          scrollX = TRUE,
          fixedColumns = list(leftColumns = 1),
          columnDefs = list(
            # onzichtbaar maken van kolommen
            list(visible = F, targets = c(2, 3, 4))
          )
        )
      ) %>%
        DT::formatStyle(
          columns = "gemeente",
          fontWeight = "bold"
        )

    })

    proxy_lijst_resultaten <- DT::dataTableProxy("lijst_resultaten")
  }


  ######################################## application ##########################################

  # Run the application
  shinyApp(ui = ui, server = server, ...)

}

