#' Title
#'
#' @param ... other parameters
#'
#' @return View with the results
#'
#' @importFrom magrittr %>%
#' @importFrom utils View
#' @importFrom utils read.csv2
#'
#' @export
#'
#' @examples
#' \dontrun{
#' explore()
#' }
explore <- function(...) {
  # gemeentelijst

  gemeenten <- c(
    "Aa en Hunze", "'s-Hertogenbosch", "Nijmegen", "Amsterdam", "Utrecht"
  )

  search_term = "adviseur"

  # read manual urls
  df.url <- read.csv2(
    system.file(file.path("dashboard", "data", "urls.csv"), package="vacancies"),
    strip.white = TRUE
  )
  names(df.url)[1] <- "gemeente"

  result <- data.frame()
  for (gemeente in gemeenten) {
    if (gemeente %in% df.url$gemeente) {
      if (df.url[df.url$gemeente == gemeente, "automatisch"] == "ja") {
        url <- data.frame(
          gemeente = gemeente,
          url = sprintf(
            df.url[df.url$gemeente == gemeente, "url"],
            search_term
          ),
          hit_order = 0
        )
      } else {
        next
      }
    } else {
      url <- find_vacancy_url(gemeente)
    }
    result <- rbind(result, url)
  }

  result <- result %>%
    dplyr::rowwise() %>%
    dplyr::mutate(
      zoekterm = {{search_term}},
      resultaat = search_vacancy(url, search_term)
    )

  View(
    result %>%
      tidyr::separate_rows(.data$resultaat, sep=";")
  )

  print("finished!")

}
