# dashboard config

peiljaar = 2022


# welke tekens worden gebruikt voor NA?
NA_vervanging <- "---"


# welke kleuren worden gebruikt?
vng_donkerblauw <- "#1A237E"
vng_marineblauw <- "#0277BD"
vng_blauw <- "#B2DFDB"
vng_lichtblauw <- "#E0F2F1"
vng_lichtgrijs <- "#757575"
vng_donkergrijs <- "#212121"
vng_mediumgrijs <- "#BDBDBD"


