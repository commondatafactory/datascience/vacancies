"
This is the UI of the GMSD selection tool
"

leftPanel <- function(parameters) {
  # left panel with mainly UI interaction
  div(
    class="flex leftflex",
    headerPanel(parameters),
    uiPanel(parameters),
    footerPanel()
  )
}


rightPanel <- function() {
  # right panel with mainly visualisations
  div(
    class="flex rightflex",

    conditionalPanel(
      "input.colofon_link % 2 == 1",
      style = "height: 100%; overflow-y: auto;",
      toelichtingsPanel(),
    ),

    conditionalPanel(
      "input.colofon_link % 2 == 0",
      style = "height: 100%; overflow-y: auto;",
      hoofdPanel()
    )
  )
}


headerPanel <- function(parameters) {
  # The header panel with title and textinput for search query

  div(
    class="header",
    div(
      class="headerbox",
      div(
        class="linker_header",
        style="display: flex; flex-wrap: wrap;",
        actionLink(
          "logo",
          img(
            src = "www/logo_vng_gs.png",
            alt = "VNG Logo",
          ),
          style = "float: left; padding-top: 6px; padding-right: 24px;"
        ),
        div(
          style = "overflow: hidden;",
          h2("Gemeentelijke vacatures"),
          p("Ontdek welke gemeenten bepaalde vacatures uit hebben staan")
        )
      ),
      # select a province
      div(
        id = "province_pickerInput",
        style = "width: 100%;",
        shinyWidgets::pickerInput(
          "provinciecode",
          label = NULL,
          choices = setNames(
            parameters[["provincies"]]$Provinciecode,
            parameters[["provincies"]]$Provincienaam
          ),
          selected = parameters[["initial_provincie"]],
          options = list(
            `live-search` = TRUE,
            `live-search-normalize` = TRUE,
            `live-search-style` = 'contains',
            `select-on-tab` = TRUE,
            title = "Kies een provincie"
          )
        )
      )
    )
  )
}


footerPanel <- function() {
  # the footer panel contains the link to the colofon

  div(
    class = "footer",
    div(
      class = "footerbox",
      div(
        class="title_footer",
        style="text-align: center;",
        span("Let op, dit is een pilot dashboard. Ontwikkeld in opdracht van VNG"),
        div(
          style="margin: 16px 0;",
          icon("envelope"),
          HTML('<a href="mailto:gripopinformatie@vng.nl", style="color: inherit; text-decoration:underline;"><b>Vragen of feedback</b></a>')
        ),
      ),
      div(
        class="links_footer",
        style="display: flex; justify-content: space-around;",
        div(
          class="linker_footer",
          icon("up-right-from-square"),
          HTML('<a href="https://vng.nl/", style="color: inherit; text-decoration:underline;">VNG</a>')
        ),
        div(
          class="rechter_footer",
          actionLink(
            "colofon_link",
            "Colofon & uitleg",
            style="margin-left: 16px; color: inherit; text-decoration: underline;",
            icon = icon("circle-info")
          )
        )
      )
    )
  )
}





toelichtingsPanel <- function() {
  documentatie <- system.file(file.path("dashboard", "documentatie"), package="vacancies")

  div(
    div(
      class = "colofonbox knoppen",
      actionLink(
        "terug_van_toelichting",
        "Terug",
        icon = icon("arrow-left")
      )
    ),
    div(
      class = "toelichtingbox toelichtingsPanel",
      fluidRow(
        column(12, h1("Colofon & uitleg")),
        column(12, includeMarkdown(file.path(documentatie, "uitleg_vacancies.md"))),
      ),

      fluidRow(
        column(12, h2("Licentie")),
        column(12, includeMarkdown(file.path(documentatie, "uitleg_licentie.md")))
      ),

      fluidRow(
        column(12, div(
          sprintf("Version: %s", pkgload::pkg_version()),
          style = "text-align: right; font-size: x-small;"
        ))
      )
    )

  )

}




uiPanel <- function(parameters) {
  div(
    id = "uiContainer",
    div(
      class = "box uiPanel",
      id = "uibox searchquery",
      h3("Zoekopties"),
      div(
        id = "query",
        style = "margin-top: 16px",
        shinyWidgets::pickerInput(
          inputId = "search_query",
          label = NULL,
          choices = parameters[["functiegroepen"]],
          selected = NULL
        ),
        # textInput(
        #   inputId = "search_query",
        #   label = NULL,
        #   value = "Zoekterm"
        # ),
        actionButton(
          inputId = "go_search",
          label = "Zoek",
          style = "width:100%;"
        ),
      ),
    ),
    div(
      class = "box reload",
      actionLink("refresh", "Reset naar beginwaarden")
    )
  )
}


hoofdPanel <- function() {
  div(
    class = "box hoofdPanel",
    #
    div(
      div(
        class = "hoofdPanel_links",
        style = "display: inline-block;",
        div(
          h3(
            textOutput("hoofdPanel_text"),
            style = "display: flex;"
          ),
          p(
            class="small_text",
            textOutput("text_aantal", inline=T)
          )
        )
      ),
      div(
        class = "overzicht_rechts",
        id = "knoppen_rechts",
        style = "display: flex; text-align: right; float: right;",
        conditionalPanel(
          "input.toggle_hoofdPanel=='Tabel'",
          div(
            id = "download_button"
          )
        ),
        # selecteer visuele representatie
        div(
          style = "display: inline-block; margin-left: 24px;",
          shinyWidgets::radioGroupButtons(
            inputId = "toggle_hoofdPanel",
            label = NULL,
            choices = c("Kaart", "Tabel"),
            status = "primary"
          )
        )
      )
    ),
    hr(
      style = "width: 100%; margin: 0;"
    ),
    conditionalPanel(
      "input.toggle_hoofdPanel=='Kaart'",
      fluidRow(
        column(
          width = 12,
          leaflet::leafletOutput("kaart_NL", height="100%")
        )
      )
    ),
    conditionalPanel(
      "input.toggle_hoofdPanel=='Tabel'",
      fluidRow(
        column(
          width = 12,
          DT::DTOutput("lijst_resultaten"),
          style = "margin-top: 16px;"
        )
      ),
    )
  )
}




colofonPanel <- function() {
  div(
    class = "colofonPanel",
    style = "margin-top: 80px;",
    div(
      class = "colofonbox",
      style = "justify-content: center; display: flex; align-items: center;",
      div(
        img(
          src = "www/logo_vng_gs.svg",
          alt = "VNG Logo",
          style = "height: 32px; margin-right: 8px;"
        )
      ),
      div(
        span("Ontwikkeld in opdracht van VNG")
      ),
      div(
        actionLink(
          "colofon_link",
          "Colofon & uitleg",
          style="margin-left: 40px; color: white; text-decoration: underline;",
          icon = icon("circle-info")
        )
      )
    )
  )
}
